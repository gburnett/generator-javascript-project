> javascript project boilerplate

## Installation

```bash
yarn global add yo
yarn global add generator-javascript-project
```

Then generate your new project:

```bash
yo @gburnett/javascript-project
```

## License

MIT © [gburnett]()
