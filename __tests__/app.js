"use strict";
const path = require("path");
const assert = require("yeoman-assert");
const helpers = require("yeoman-test");

describe("generator-javascript-project:app", () => {
  describe("in a yarn project", () => {
    beforeEach(() => {
      return helpers
        .run(path.join(__dirname, "../generators/app"))
        .withPrompts({ packageManager: "yarn" });
    });

    it("creates files", () => {
      assert.file([".tern-project", ".indium.json", ".npmrc"]);
      assert.fileContent("package.json", /yarn/);
      assert.fileContent("package.json", /prettier/);
      assert.fileContent("package.json", /husky/);
      assert.fileContent("package.json", /lint-staged/);
    });
  });

  describe("in an npm project", () => {
    beforeEach(() => {
      return helpers
        .run(path.join(__dirname, "../generators/app"))
        .withPrompts({ packageManager: "npm" });
    });

    it("creates files for an npm project", () => {
      assert.file([".tern-project", ".indium.json", ".npmrc"]);
      assert.fileContent("package.json", /npm run/);
      assert.fileContent("package.json", /prettier/);
      assert.fileContent("package.json", /husky/);
      assert.fileContent("package.json", /lint-staged/);
    });
  });
});
