"use strict";
const Generator = require("yeoman-generator");
const extend = require("deep-extend");

const packageTemplates = { yarn: "yarn-package.json", npm: "npm-package.json" };

module.exports = class extends Generator {
  async prompting() {
    this.answers = await this.prompt([
      {
        type: "list",
        name: "packageManager",
        message: "Which package manager?",
        choices: ["yarn", "npm"],
        store: true
      }
    ]);
  }

  writing() {
    const packageTemplate = packageTemplates[this.answers.packageManager];

    const templates = [".npmrc", ".indium.json", ".tern-project"];
    templates.forEach(template =>
      this.fs.copy(this.templatePath(template), this.destinationPath(template))
    );

    const pkg = this.fs.readJSON(this.destinationPath("package.json"), {});
    const javascriptPkg = this.fs.readJSON(this.templatePath(packageTemplate));

    extend(pkg, javascriptPkg);

    this.fs.writeJSON(this.destinationPath("package.json"), pkg);
  }

  install() {
    if (this.answers.packageManager === "yarn") {
      this.yarnInstall(["prettier", "lint-staged", "husky"], { dev: true });
    } else if (this.answers.packageManager === "npm") {
      this.npmInstall(["prettier", "lint-staged", "husky"], { only: "dev" });
    } else {
      throw new Error("Whoops! No package manager!");
    }
  }
};
